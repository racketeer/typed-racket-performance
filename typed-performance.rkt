#lang racket/base

(require (prefix-in untyped: "unsafe-color.rkt")
         (prefix-in unsafe-provided: "typed-color-unsafe-provide.rkt")
         (prefix-in typed: "typed-unsafe-color.rkt")
         (prefix-in typed-orig: "typed-color.rkt")
         (prefix-in plain: "plain-color.rkt"))

;(typed:rgb-average 'test 'it)

(displayln '===rgb-average)

(for ((i 2))

  (displayln '---)

  (display "typed: ")
  (time
   (for ((i (in-range 0 10000000)))
     (typed:rgb-average #xff00ff #x00ff00)))

  (display "typed-orig: ")
  (time
   (for ((i (in-range 0 10000000)))
     (typed-orig:rgb-average #xff00ff #x00ff00)))

  (display "unsafe: ")
  (time
   (for ((i (in-range 0 10000000)))
     (untyped:rgb-average #xff00ff #x00ff00)))

  (display "typed, unsafe provided: ")
  (time
   (for ((i (in-range 0 10000000)))
     (unsafe-provided:rgb-average #xff00ff #x00ff00)))

  (display "plain: ")
  (time
   (for ((i (in-range 0 10000000)))
     (plain:rgb-average #xff00ff #x00ff00)))

  )

(displayln '===rgb-distance^2)

(for ((i 2))

  (displayln '---)

  (display "typed: ")
  (time
   (for ((i (in-range 0 10000000)))
     (typed:rgb-distance^2 #xff00ff #x00ff00)))

  (display "typed-orig: ")
  (time
   (for ((i (in-range 0 10000000)))
     (typed-orig:rgb-distance^2 #xff00ff #x00ff00)))

  (display "unsafe: ")
  (time
   (for ((i (in-range 0 10000000)))
     (untyped:rgb-distance^2 #xff00ff #x00ff00)))

  (display "typed, unsafe provided: ")
  (time
   (for ((i (in-range 0 10000000)))
     (unsafe-provided:rgb-distance^2 #xff00ff #x00ff00)))

  (display "plain: ")
  (time
   (for ((i (in-range 0 10000000)))
     (plain:rgb-distance^2 #xff00ff #x00ff00)))

  )
